#include <stdio.h>

//the following program shows the relation between the ticket price and the profit made

//functions declaration
int getProfit(int p);
int getRevenue(int q);
int getCost(int r);
int getAttendance(int s);


//this function calculates the profit made at each ticket value
int getProfit(int p){
	return getRevenue(p)-getCost(p);
}
//this function calculates the total revenue 
int getRevenue(int q){
	return q*getAttendance(q);
}

//this function calculates the cost for the movie 
int getCost(int r){
	return 500+getAttendance(r)*3;
}

//this function calculates the number of attendees for the movie at each ticket price
int getAttendance(int s){

	//the below equation is a relationship between the ticket price and the number of attendees
	return 120-(s-15)/5*20 ;	
}



int main()
{

	int price;
	int max1=0,max2=0;
	printf("The Profit for each ticket price: \n\n");

	//checks for all prices from 5- 50
	for(price=5;price<50;price+=5)
	{
		printf("Ticket Price: Rs.%d \t\t Profit: Rs.%d\n\n",price,getProfit(price));
		if(getProfit(price)>=max1)
		{	
			//find out the highest profit and the corresponding ticket price
			max1=getProfit(price);
			max2= price;
		}

	} 

	printf("The highest profit is Rs.%d\n",max1);
	printf("The best ticket price is Rs.%d\n",max2);
	
	return 0;
}

