//Write a C program to reverse a sentence entered by user.



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
 
#define MAX_SIZE 1000

int main()
{
    char s[MAX_SIZE];  
    int i,n,temp;
 
    printf("Enter Sentence : ");
    //declare the characater array
    gets(s);

    //get length of array using strlen
    n=strlen(s);
    
    printf("Sentence before reverse = %s\n",s);
 
    //reversing each character by index
    for(i=0;i<n/2;i++)  
    {   
    

    	temp=s[i];
    	s[i]=s[n-i-1];
    	s[n-i-1]=temp;
 
 	}
 	 
     
    printf("Sentence after reverse = %s\n",s);
    
    return 0;
}
