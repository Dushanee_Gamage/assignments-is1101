//Write a program to add an multiply two given matrixes.

#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

int main()
{
	int a[MAX_SIZE][MAX_SIZE], b[MAX_SIZE][MAX_SIZE],c[MAX_SIZE][MAX_SIZE]={0}, d[MAX_SIZE][MAX_SIZE]={0};
	int i,j,k,m,n,p,q;

	printf("Enter number of rows and columns in matrix A: ");
	scanf("%d%d",&m,&n);
	printf("Enter number of rows and columns in matrix B: ");
	scanf("%d%d",&p,&q);

	if(m!=p || n!=q)
	{
		printf("Cannot add the two matrices");
		
	}
	else if(n!=p)
	{
		printf("Cannot multiply the two matrices");
		
	}
	else
	{   
        //get matrix A
		printf("Enter elements of matrix A: \n");
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				scanf("%d", &a[i][j]);

        //get matrix B
		printf("Enter elements of matrix B: \n");
		for(i=0;i<p;i++)
			for(j=0;j<q;j++)
				scanf("%d", &b[i][j]);


		//Matrix Addition
		for(i=0;i<m;i++)
            //store added elements in an array
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("Result of Matrix Addition:\n");
        //traverse the added elements
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}

		//Matrix Multiplication
		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
                //store the multiplied elements in an array
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("Result of Matrix Multiplication:\n");
        //traverse the multiplied elements from the stored array
		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}

    return 0;
	
}
