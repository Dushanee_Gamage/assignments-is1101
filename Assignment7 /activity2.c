//Write a program to find the Frequency of a given character in a given string.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_SIZE 1000

int main()
{
    char string[MAX_SIZE],ch;
  
    //get string from user
    printf("Enter String : \n");
    gets(string);
    
    for(ch ='a';ch<='z';ch++)
    {
        int count=0;

        //store the frequency of each character from a-z
        for(int i =0; i<strlen(string);i++)
        {
            if(ch==string[i])
            {
                count++;
            }
        }
        if(count>0)
        {
            printf("The frequency of %c is - %d \n",ch,count);
        }
    }


    return 0;

}
