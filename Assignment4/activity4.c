#include <stdio.h>

//this program finds factors of a given number

int main()
{
   int  num,i;
   printf("Please enter number: ");
   scanf("%d", &num);

    printf("The factors of %d are :",num);

   for(i=1; i<=num;i++)
   {
      if(num%i==0)
      {
          printf(" %d,",i);
      }
   }
    printf("\n");
     return 0;
   
}
