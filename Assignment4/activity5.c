#include <stdio.h>

//this program prints multiplication tables from 1 to n(given integer)

int main()
{
   int  n,i,j;
   printf("Please enter number: ");
   scanf("%d", &n);

    printf("The multiplication tables from 1 to %d are : \n",n);

   for(i=1; i<=n;i++)
  
   {  
      for(j=1;j<=12;j++)
      {
          printf(" %d * %d = %d \n",i,j,i*j);
         
      }

       printf("\n");
     
      
   }
    
     return 0;
   
}
