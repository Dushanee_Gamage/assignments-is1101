#include<stdio.h>

//this program checks whether the given number is positve or negative or zero

int main()
{
	int num;
	printf("Enter Number :");
	scanf("%d",&num);
	if(num>0)
	{
		printf("%d is a Positive Number\n",num);
	}
	else if(num<0)
	{
		printf("%d is a Negative Number\n",num);
	}
	else
	{
		printf("Zero\n");
	}
	return 0;

}
