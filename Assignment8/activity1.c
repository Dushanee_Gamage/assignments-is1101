//Write a program to store the details of students

#include <stdio.h>
#define MAX_SIZE 100

//define structure

struct 
{
    char firstName[MAX_SIZE];
    char subject[MAX_SIZE];
    float marks;
    
}   info[MAX_SIZE];

int main() 
{

    int i;

    // get student information from user
    for (i = 0; i < 5; ++i) 
    {
 
        printf("---Student %d---\n", i+1);
        printf("Enter first name: ");
        scanf("%s", info[i].firstName);
        printf("Enter subject: ");
        scanf("%s", info[i].subject);
        printf("Enter marks: ");
        scanf("%f", &info[i].marks);

        printf("\n");
    }


    printf("\n\nStudent Information: \n");

    //display entered student information

    for (i = 0; i < 5; ++i) 
    {
        printf("\n Student %d\n", i + 1);
        printf("First name: ");
        puts(info[i].firstName);
        printf("Subject: ");
        puts(info[i].subject);
        printf("Marks: %.2f\n", info[i].marks);
        
        
    }
    return 0;
}



