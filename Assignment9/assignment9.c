#include <stdio.h>
#include <string.h>
#define MAX_SIZE 100

int main()
{
    char word[MAX_SIZE];
    FILE *file_ptr;

    file_ptr = fopen("assignment9.txt", "w");
    fprintf(file_ptr, "UCSC is one of the leading institutes in Sri Lanka for computing studies. \n");
    fclose(file_ptr);

    file_ptr = fopen("assignment9.txt", "r");
    fgets(word, 100, file_ptr);
    puts(word);
    fclose(file_ptr);

    file_ptr = fopen("assignment9.txt", "a");
    fprintf(file_ptr, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields");
    fclose(file_ptr);

    return 0;
}
