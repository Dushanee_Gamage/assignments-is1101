#include<stdio.h>

// set the star value to 0
int c=0;


//functions prototype
int fibonacci(int);
void fibonacciSeq(int);

//function to get the fibonacci numbers
int fibonacci(int x){
    if(x==0){
        return 0;
    }
    else if (x==1){
        return 1;
    }
    else{
        return fibonacci(x-1) + fibonacci(x-2);
    }
}

//function to print the sequence from 0 to n
void fibonacciSeq(int n){
    if(c<=n)
    {
        printf("%d\n",fibonacci(c));
    }
    c++;
    fibonacciSeq(n);


}




int main(){
    int n;
    printf("enter number\n");
    scanf("%d",&n);
    fibonacciSeq(n);

    return 0;
}
