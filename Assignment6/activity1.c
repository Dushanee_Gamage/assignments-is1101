// this program print a number triangle using recursive functions

#include <stdio.h>

//function prototype
void printRow(int);
void pattern(int);

//set the start as 1
int c=1; 

//print the numbers in each row
void printRow (int c)
{
	if (c>0)
	{
	    printf("%d",c);

        //recurse the same function
	    printRow(c-1);
	}
}

//prints the triangle
void pattern(int n)                            	
{
	if(n>0)
	{
		printRow(c);
		printf("\n");
		c++;
		pattern(n-1);
	}
}

int main()
{
	int n;
	printf("Enter number of rows:");
	scanf("%d",&n);
	pattern(n);

	return 0;
}

